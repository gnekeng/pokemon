
public class Pokeball extends Item{

    public Pokeball(String name, int quantity) {
        super(name, quantity);
        ability = "Can Catch Pokemon Success 50%";
        cost = 50;
        imageUrl = "pokeball.png";
    }

    public void use(Trainer user, Pokemon pokemon){
        int successPercentage = (int)(Math.random() * 99);
        successPercentage += 100 - (int)(pokemon.getHp()*100/pokemon.getMaxHp());
        quantity--;
        if(successPercentage > 50){
            user.catchPokemon(pokemon);
        }
        else{
            pokemon.choiceRandom(user.getCompanionPokemon());
            if(pokemon.isWinner(user.getCompanionPokemon())){
                new ResultBattle(user);
            }
            else{
                Battle btp = new Battle(user.getCompanionPokemon(), pokemon, user);
            }
            
        }
    }

}