
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ChosePokemon extends JFrame{
    
    public ChosePokemon(Trainer trainer, String action){
        super("ChosePokemon");

        JPanel main = new JPanel(new GridLayout(4, 2));
        add(main);

        for(Pokemon pokemon: trainer.getPokemons()){
            JPanel subPanel = new JPanel(new GridLayout(1, 2));
            main.add(subPanel);
            JButton pokemonButton = new JButton();
            subPanel.add(pokemonButton);
            if(action == "choseBattle"){
                if(pokemon.isAlive()){
                    pokemonButton.addActionListener(new ActionListener(){
                        public void actionPerformed(ActionEvent e){
                            trainer.companionPokemon(pokemon);
                            Battle bt = new Battle(pokemon, trainer.getEnemyPokemon(), trainer);
                            setVisible(false);
                        }
                    });
                }
            }
            else if(action == "changePokemon"){
                pokemonButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        trainer.companionPokemon(pokemon);
                        Battle bt = new Battle(pokemon, trainer.getEnemyPokemon(), trainer);
                        trainer.getEnemyPokemon().attack(pokemon);
                        setVisible(false);
                    }
                });
            }
            else if(action == "replacement"){
                pokemonButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        trainer.pokemonReplacement(pokemon, trainer.getEnemyPokemon());
                        setVisible(false);
                    }
                });
            }
            else if(action == "useItemInBattle"){
                pokemonButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        trainer.useItem(trainer.getHoldItem(), pokemon);
                        trainer.getEnemyPokemon().attack(trainer.getCompanionPokemon());
                        Battle btp = new Battle(trainer.getCompanionPokemon(), trainer.getEnemyPokemon(), trainer);
                        setVisible(false);
                    }
                });
            }
            else if(action == "useItemOutBattle"){
                pokemonButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        trainer.useItem(trainer.getHoldItem(), pokemon);
                        MainGame mg = new MainGame(trainer);
                        setVisible(false);
                    }
                });
            }
            else if(action == "checkStatus"){
                pokemonButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        PokemonStatus stat = new PokemonStatus(pokemon, trainer);
                        setVisible(false);
                    }
                });
            }
            JLabel pokemonLabel = new JLabel(pokemon.getName() + " Hp: " + pokemon.getHp() + "/" + pokemon.getMaxHp() + 
                                    " Lv." + pokemon.getLevel());
            pokemonButton.add(pokemonLabel);
            pokemonButton.setIcon(pokemon.getImageIcon());
        }
        JButton cancleButton = new JButton("Cancle");
        if(action == "choseBattle"){
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    MainGame mg = new MainGame(trainer);
                    setVisible(false);
                }
            });
        }
        else if(action == "changePokemon"){
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    Battle bt = new Battle(trainer.getCompanionPokemon(), trainer.getEnemyPokemon(), trainer);
                    setVisible(false);
                }
            });
        }
        else if(action == "replacement"){
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    MainGame mg = new MainGame(trainer);
                    setVisible(false);
                }
            });
        }
        else if(action == "useItemInBattle"){
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    OpenBag bagButton = new OpenBag(trainer, "inBattle");
                    setVisible(false);
                }
            });
        }
        else if(action == "useItemOutBattle"){
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    OpenBag bagButton = new OpenBag(trainer, "outBattle");
                    setVisible(false);
                }
            });
        }
        else if(action == "checkStatus"){
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    MainGame mg = new MainGame(trainer);
                    setVisible(false);
                }
            });
        }
        main.add(cancleButton);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}