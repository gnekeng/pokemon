
public class Charmeleon extends Charmander{
    public Charmeleon(String name){
        super(name, (int)(Math.random() * 35) + 1, (int)(Math.random() * 36 ) + 1);
        imageUrl = "charmeleon.png";
        evolveLevel = 36;
    }
    
    public Charmeleon(String name, int maxHp, int level){
        super(name, maxHp, level);
        imageUrl = "charmeleon.png";
        evolveLevel = 36;
    }
    
    protected  Pokemon evolve(){
        Charizard charizard = new Charizard(this.getName(), this.getMaxHp(), this.getLevel());
        System.out.println(this.getName() + "Evolvutions");
        return charizard;
    }

    public void selfHeal(){
        int value = (int)(level + maxHp * (1/10));
        hp = hp + value;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Self Heal!");
    }

    public void levelUp(int num){
        level = level + 1*num;
        maxHp = maxHp + 5*num;
        hp = hp + 5*num;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Level UP!");
    }

    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + name + " attack " + enemy.getName());
        enemy.damage(16 + (int)(0.5 * level));   
    }
}