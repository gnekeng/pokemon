
import java.util.*;

public class Trainer{
    private Scanner sc;
    private ArrayList<Pokemon> pokemons;
    private String name;
    private int coin;
    private ArrayList<Item> bag;
    private int numPokemon;
    private Pokemon enemyPokemon;
    private Pokemon companionPokemon;
    private Item holdItem;

    public Trainer(String name){
        sc = new Scanner(System.in);
        pokemons = new ArrayList<Pokemon>();
        pokemons.add(new Pikachu("Pikachu"));
        // bag.add(new Pokeball("Pokeball", 5));
        bag = new ArrayList<Item>();
        bag.add(new Pokeball("Pokeball", 5));
        bag.add(new Potion("Potion", 5));

        this.name = name;
        this.coin = 200;
        this.numPokemon = 1;
    }
    
    public String getName(){
        return this.name;
    }

    public int getCoin(){
        return coin;
    }

    public Pokemon getEnemyPokemon(){
        return enemyPokemon;
    }

    public Pokemon getCompanionPokemon(){
        return companionPokemon;
    }

    public ArrayList<Item> getBag(){
        return bag;
    }

    public Item getHoldItem(){
        return holdItem;
    }

    public void useItem(Item item, Pokemon pokemon){
        item.use(this, pokemon);
    }

    public void buyItem(Item item){
        Boolean isDeal = false;
        if(coin >= item.getCost()){
            coin -= item.getCost();
            for(Item myItem: bag){
                if(myItem.getClass().equals(item.getClass())){
                    myItem.quantity += item.quantity;
                    isDeal = true;
                    System.out.println("Trainer buy " + item.getName());
                }
            }

            if(!isDeal){
                bag.add(item);
            }
        }

    }

    public void companionPokemon(Pokemon pokemon){
        companionPokemon = pokemon;
    }

    public void holdItem(Item item){
        holdItem = item;
    }
    
    public Boolean readyBattle(){
        int countPokemon = 0;
        for(Pokemon pokemon : pokemons) {
            if(pokemon.isAlive()){
                countPokemon++;
            }
        }
        if(countPokemon > 0){
            return true;
        }
        coin = coin - 100;
        if(coin < 0){
            coin = 0;
        }
        return false;
    }

    public void goHospital(ArrayList<Pokemon> pokemons){
        for(Pokemon pokemon: pokemons){
            pokemon.hp = pokemon.maxHp;
        }
        coin = coin - 10;
        if(coin < 0){
            coin = 0;
        }
        printPokemon(pokemons);
        MainGame mg = new MainGame(this);
    }

    protected void catchPokemon(Pokemon pokemon){
        if(!pokemonFull()){
            pokemons.add(pokemon);
        }
        else{
            ChosePokemon chp = new ChosePokemon(this, "replacement");
        }
        numPokemon++;
        ChangePokemonName chpn = new ChangePokemonName(this, pokemon);
    }

    protected void pokemonReplacement(Pokemon oldPokemon, Pokemon newPokemon){
        int index = pokemons.indexOf(oldPokemon);
        pokemons.set(index, newPokemon);
    }

    public Boolean pokemonFull(){
        if(numPokemon < 6){
            return false;
        }
        return true;
    }

    protected void evolvePokemon(Pokemon pokemon){
        int index = pokemons.indexOf(pokemon);
        pokemon = pokemon.evolve();
        pokemons.set(index, pokemon);
    }

    protected void collectCoin(Pokemon enemy){
        coin += 2 * enemy.getLevel();
    }

    public void meetPokemon(){
        Pokemon wildPokemon = PokemonRandomizer.getPokemon();
        enemyPokemon = wildPokemon;
    }
    
    public void printPokemon(ArrayList<Pokemon> pokemons){
        int number = 0;
        for(Pokemon p: pokemons){
            System.out.println("" + number + " " + p + " HP " + p.getHp());
            number++;
        }
    }

    public ArrayList<Pokemon> getPokemons(){
        return pokemons;
    }
    // public void play(){
    //     String cmd = "";

    //     do{
    //         System.out.print("Enter cmd: ");
    //         cmd = sc.nextLine();
    //         if(cmd.equals("print")){
    //             printPokemon(bag);
    //         }
    //         else if(cmd.equals("catch")){
    //             catchPokemon();
    //         }
    //     }while(!cmd.equals("quit"));
    // }

    // public void catchPokemon(){
    //     System.out.println("Catch Pokemon");
    //     ArrayList<Pokemon> pokemons = PokemonRandomizer.getPokemon(5);

    //     System.out.println("Pokemon arround you");
    //     int no = 0;
    //     printPokemon(pokemons);
        
    //     System.out.println("\n\nSelect pokemon or run(-1)");
    //     no = sc.nextInt();
    //     if(no < 0)
    //         return;
    //     Pokemon wildPokemon = pokemons.get(no);

    //     System.out.println("\n\npokemon in bag: ");
    //     printPokemon(bag);
    //     System.out.println("Select pokemon in bag: ");
        
    //     no = sc.nextInt();
    //     Pokemon myPokemon = bag.get(no);

    //     boolean isWin = false;
    //     do{
    //         myPokemon.attack(wildPokemon);
    //         if(wildPokemon.getHp() == 0){
    //             isWin = true;
    //             break;
    //         }
    //         wildPokemon.attack(myPokemon);
    //         if(myPokemon.getHp() == 0){
    //             isWin = false;                    
    //             break;
    //         }
    //     } while(true);

    //     if(isWin){
    //         System.out.println("You catch: " + wildPokemon.getName());
    //         bag.add(wildPokemon);
    //     }
    //     else{
    //         System.out.println(wildPokemon.getName() + " win");
    //     }

    //     sc.nextLine();
    // }
}