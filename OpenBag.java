
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class OpenBag extends JFrame {
    public OpenBag(Trainer trainer, String action){
        super("OpenBag");

        JPanel main = new JPanel(new GridLayout(3, 2));
        add(main);

        for(Item item: trainer.getBag()){
            JPanel subPanel = new JPanel(new GridLayout(1, 2));
            main.add(subPanel);
            JButton useItemButton = new JButton();
            subPanel.add(useItemButton);
            JLabel itemLabel = new JLabel(item.getName() + " x " + item.getQuantity());
            useItemButton.add(itemLabel);;

            if(item.getQuantity() > 0){
                if(action == "inBattle"){
                    useItemButton.addActionListener(new ActionListener(){
                        public void actionPerformed(ActionEvent e){
                            if(item instanceof Pokeball){
                                trainer.useItem(item, trainer.getEnemyPokemon());
                            }
                            else{
                                trainer.holdItem(item);
                                ChosePokemon chp = new ChosePokemon(trainer, "useItemInBattle");
                            }
                            setVisible(false);
                        }
                    });
                }
                else if(action == "outBattle"){
                    if(!(item instanceof Pokeball)){
                        useItemButton.addActionListener(new ActionListener(){
                            public void actionPerformed(ActionEvent e){
                                trainer.holdItem(item);
                                ChosePokemon chp = new ChosePokemon(trainer, "useItemOutBattle");
                                setVisible(false);
                            }
                        });
                    }
                }
            }
            useItemButton.setIcon(item.getImageIcon());
        }
        if(action == "inBattle"){
            JButton cancleButton = new JButton("Cancle");
            main.add(cancleButton);
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    Battle btp = new Battle(trainer.getCompanionPokemon(), trainer.getEnemyPokemon(), trainer);
                    setVisible(false);
                }
            });
        }
        else if(action == "outBattle"){
            JButton cancleButton = new JButton("Cancle");
            main.add(cancleButton);
            cancleButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    MainGame mg = new MainGame(trainer);
                    setVisible(false);
                }
            });
        }

            

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}