
public class Potion extends Item{
    public Potion(String name, int quantity) {
        super(name, quantity);
        ability = "Heal pokemon 25 units%";
        cost = 25;
        imageUrl = "potion.png";
    }

    public void use(Trainer user, Pokemon pokemon){
        pokemon.heal(25);
        quantity--;
    }
}