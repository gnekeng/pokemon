
public class Kabigon extends Pokemon {
    public Kabigon(String name){
        super(name, (int)(Math.random() * 50) + 1, (int)(Math.random() * 49 ) + 1);
        imageUrl = "kabigon.png";
        evolveLevel = 0;
    }

    public Kabigon(String name, int maxHp, int level){
        super(name, maxHp, level);
        imageUrl = "kabigon.png";
        evolveLevel = 0;
    }
    
    protected  Pokemon evolve(){
        return null;
    }
    
    public void levelUp(int num){
        level = level + 1*num;
        maxHp = maxHp + 4*num;
        hp = hp + 4*num;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Level UP!");
    }

    public void selfHeal(){
        int value = (int)(level + maxHp * (1/10));
        hp = hp + value;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Self Heal!");
    }

    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + name + " attack " + enemy.getName());
        enemy.damage(5 + (int)(0.5 * level));   
    }
}