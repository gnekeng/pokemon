
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ChangePokemonName extends JFrame{
    public ChangePokemonName(Trainer trainer, Pokemon pokemon){
        JPanel main = new JPanel(new GridLayout(4, 2));
        add(main);

        JPanel subPanel = new JPanel(new GridLayout(1, 2));
        main.add(subPanel);
        JTextField changeField = new JTextField();
        changeField.setText(pokemon.getName());
        subPanel.add(changeField);
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(!changeField.getText().equals("")){
                    pokemon.changeName(changeField.getText());
                    MainGame mg = new MainGame(trainer);
                    setVisible(false);
                }
            }
        });
        subPanel.add(submitButton);
            
        JButton cancleButton = new JButton("Cancle");
        cancleButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ChosePokemon chp = new ChosePokemon(trainer, "checkStatus");
                setVisible(false);
            }
        });
        main.add(cancleButton);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}