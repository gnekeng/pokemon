
public class Raichu extends Pikachu{
    public Raichu(String name){
        super(name, (int)(Math.random() * 35) + 1, (int)(Math.random() * 49 ) + 1);
        imageUrl = "raichu.png";
        evolveLevel = 0;
    }
    
    public Raichu(String name, int maxHp, int level){
        super(name, maxHp, level);
        imageUrl = "raichu.png";
        evolveLevel = 0;
    }
    
    protected  Pokemon evolve(){
        return null;
    }

    public void selfHeal(){
        int value = (int)(level + maxHp * (1/10));
        hp = hp + value;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Self Heal!");
    }

    public void levelUp(int num){
        level = level + 1*num;
        maxHp = maxHp + 5*num;
        hp = hp + 5*num;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Level UP!");
    }

    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + name + " attack " + enemy.getName());
        enemy.damage(20 + (int)(0.5 * level));   
    }
}