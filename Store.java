
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Store extends JFrame{
    public ArrayList<Item> store;

    public Store(Trainer trainer){
        super("Store");
        this.store = new ArrayList<Item>();
        this.store.add(new Pokeball("Pokeball", 0));
        this.store.add(new Potion("Potion", 0));

        JPanel main = new JPanel(new GridLayout(3, 2));
        add(main);

        JPanel numCoinPanel = new JPanel(new GridLayout(1, 2));
        main.add(numCoinPanel);
        JLabel numCoinLabel = new JLabel("Coin : " + trainer.getCoin());
        numCoinPanel.add(numCoinLabel);
        numCoinLabel.setHorizontalAlignment(SwingConstants.CENTER);

        JPanel notificationPanel = new JPanel(new GridLayout(1, 2));
        main.add(notificationPanel);
        JLabel notificationLabel = new JLabel();
        notificationPanel.add(notificationLabel);
        notificationLabel.setHorizontalAlignment(SwingConstants.CENTER);

        JTextField numField = new JTextField();
        numField.setText("1");
        main.add(numField);

        for(Item item: this.store){
            JPanel subPanel = new JPanel(new GridLayout(1, 2));
            main.add(subPanel);
            JButton itemButton = new JButton();
            subPanel.add(itemButton);
            itemButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    try {
                        int numItem = Integer.parseInt(numField.getText());
                        item.updateQuantity(numItem);
                        if(trainer.getCoin() >= item.getCost()){
                            notificationLabel.setText("You get " + numItem + " " + item.getName());
                        }
                        else{
                            notificationLabel.setText("Not enough for " + numItem + " " + item.getName());
                        }
                        trainer.buyItem(item);
                        numCoinLabel.setText("Coin : " + trainer.getCoin());
                    } catch (Exception ae) {
                        System.out.println(ae);
                    }
                }
            });
            JLabel itemLabel = new JLabel(item.getName());
            itemButton.add(itemLabel);
            itemButton.setIcon(item.getImageIcon());
        }
        
        JButton cancleButton = new JButton("Cancle");
        cancleButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                MainGame mg = new MainGame(trainer);
                setVisible(false);
            }
        });

        main.add(cancleButton);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}