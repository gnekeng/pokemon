
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Battle extends JFrame{
    // private Pokemon myPokemon;
    // private Pokemon enemy;
    // private Trainer trainer;

    public Battle(Pokemon myPokemon, Pokemon enemy, Trainer trainer){
        super(trainer.getName() + " Vs " + enemy.getName());
        // this.enemy = enemy;
        // this.trainer = trainer;
        
        JPanel main = new JPanel(new GridLayout(3, 2));

        JPanel enemyPanel = new JPanel(new GridLayout(1, 2));
        JLabel enemyInfo = new JLabel(enemy.getName() + " HP: " + enemy.getHp() + " Lv. " + enemy.getLevel());

        JPanel myPokemonPanel = new JPanel(new GridLayout(1, 2));
        JLabel myPokemonInfo = new JLabel(myPokemon.getName() + " HP: " + myPokemon.getHp() + " Lv. " + myPokemon.getLevel());

        JPanel controlJPanel = new JPanel(new GridLayout(1, 4));
        JButton atkButton = new JButton("ATTACK");
        atkButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                myPokemon.attack(enemy);
                enemyInfo.setText(enemy.getName() + " HP: " + enemy.getHp() + " Lv. " + enemy.getLevel());
                if(myPokemon.isWinner(enemy)){
                    trainer.collectCoin(enemy);
                    if(myPokemon.canEvolve()){
                        trainer.evolvePokemon(myPokemon);
                    }
                    new ResultBattle(trainer);
                    setVisible(false);
                }
                else{
                    enemy.choiceRandom(myPokemon);
                    myPokemonInfo.setText(myPokemon.getName() + " HP: " + myPokemon.getHp() + " Lv. " + myPokemon.getLevel());
                    if(enemy.isWinner(myPokemon)){
                        new ResultBattle(trainer);
                        setVisible(false);    
                    }
                }
            }   
        });

        JButton bagButton = new JButton("BAG");
        bagButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                myPokemonInfo.setText(myPokemon.getName() + " HP: " + myPokemon.getHp() + " Lv. " + myPokemon.getLevel());
                new OpenBag(trainer, "inBattle");
                setVisible(false);
            }   
        });

        JButton pokemonButton = new JButton("POKEMON");
        pokemonButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                myPokemonInfo.setText(myPokemon.getName() + " HP: " + myPokemon.getHp() + " Lv. " + myPokemon.getLevel());
                if(trainer.readyBattle()){
                    new ChosePokemon(trainer, "changePokemon");
                }
                setVisible(false);
                

            }   
        });
        
        JButton runButton = new JButton("RUN");
        runButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Boolean success = (int)(Math.random()) * 99 < 80;
                if(success){
                    new ResultBattle(trainer);
                    setVisible(false);
                }
                else{
                    enemy.choiceRandom(myPokemon);
                    myPokemonInfo.setText(myPokemon.getName() + " HP: " + myPokemon.getHp() + " Lv. " + myPokemon.getLevel());
                    if(enemy.isWinner(myPokemon)){
                        new ResultBattle(trainer);
                        setVisible(false);
                    }
                }
            }   
        });

        add(main);

        main.add(enemyPanel);
        enemyPanel.add(enemyInfo);
        enemyPanel.add(enemy.getImage());
        
        main.add(controlJPanel);
        controlJPanel.add(atkButton);
        controlJPanel.add(bagButton);
        controlJPanel.add(pokemonButton);
        controlJPanel.add(runButton);

        main.add(myPokemonPanel);
        myPokemonPanel.add(myPokemon.getImage());
        myPokemonPanel.add(myPokemonInfo);

        enemyInfo.setHorizontalAlignment(SwingConstants.CENTER);
        myPokemonInfo.setHorizontalAlignment(SwingConstants.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 350);
        setVisible(true);
    }
}