
import java.awt.*;
import javax.swing.ImageIcon;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

public abstract class Item {
    protected String name;
    protected int quantity;
    protected String ability;
    protected int cost;
    protected String imageUrl;

    public Item(String name, int quantity){
        this.name = name;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public int getQuantity(){
        return quantity;
    }

    public String getAbility(){
        return ability;
    }

    public int getCost(){
        return cost * quantity;
    }
    
    public ImageIcon getImageIcon(){
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(imageUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image dimg = img.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
        ImageIcon imageIcon = new ImageIcon(dimg);
        return imageIcon;
    }

    public void updateQuantity(int quantity){
        this.quantity = quantity;
    }

    public Boolean haveInBag(){
        if(quantity!=0){
            return true;
        }
        return false;
    }

    public abstract void use(Trainer user, Pokemon pokemon);

}