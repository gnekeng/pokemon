
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ResultBattle extends JFrame{

    public ResultBattle(Trainer trainer){
        super("ResultBattle");

        JPanel main = new JPanel(new GridLayout());
        add(main);
        
        if(trainer.readyBattle()){
            if(!trainer.getEnemyPokemon().isAlive()){
                JPanel resultPanel = new JPanel();
                main.add(resultPanel);
                JButton resultButton = new JButton("You defeat " + trainer.getEnemyPokemon().getName());
                resultPanel.add(resultButton);
                resultButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        new MainGame(trainer);
                        setVisible(false);
                    }
                });
            }
            else if(trainer.getEnemyPokemon().isAlive()){
                JPanel resultPanel = new JPanel();
                main.add(resultPanel);
                JButton resultButton = new JButton("You run form "+ trainer.getEnemyPokemon().getName());
                resultPanel.add(resultButton);
                resultButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        new MainGame(trainer);
                        setVisible(false);
                    }
                });
            }
            else if(!trainer.getCompanionPokemon().isAlive()){
                JPanel resultPanel = new JPanel();
                main.add(resultPanel);
                JButton resultButton = new JButton(trainer.getCompanionPokemon() + "not is alive");
                resultPanel.add(resultButton);
                resultButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        new ChosePokemon(trainer, "choseBattle");
                        setVisible(false);
                    }
                });
            }

        }
        else{
            JPanel resultPanel = new JPanel();
            main.add(resultPanel);
            JButton resultButton = new JButton("Your all pokemon is not alive Go to HOSPITAL!!");
            resultPanel.add(resultButton);
            resultButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    trainer.goHospital(trainer.getPokemons());
                    setVisible(false);
                }
            });
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}