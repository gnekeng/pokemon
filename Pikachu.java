
public class Pikachu extends Pokemon{

    public Pikachu(String name){
        super(name, (int)(Math.random() * 25) + 1, (int)(Math.random() * 16 ) + 1);
        imageUrl = "pikachu.png";
        evolveLevel = 21;
    }

    public Pikachu(String name, int maxHp, int level){
        super(name, maxHp, level);
        imageUrl = "pikachu.png";
        evolveLevel = 21;
    }

    protected  Pokemon evolve(){
        Raichu raichu = new Raichu(this.getName(), this.getMaxHp(), this.getLevel());
        System.out.println(this.getName() + "Evolvutions");
        return raichu;
    }

    public void levelUp(int num){
        System.out.println(num);
        level = level + 1*num;
        maxHp = maxHp + 2*num;
        hp = hp + 2*num;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Level UP!");
    }

    public void selfHeal(){
        int value = (int)(level + maxHp * (1/10));
        hp = hp + value;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Level UP!");
    }

    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + name + " attack " + enemy.getName());
        enemy.damage(10 + (int)(0.5 * level));   
    }


}