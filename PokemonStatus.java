
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PokemonStatus extends JFrame{
    public PokemonStatus(Pokemon pokemon, Trainer trainer){
        super("Pokemon Status: " + pokemon.getName());

        JPanel main = new JPanel(new GridLayout(3, 2));
        add(main);
        
        JPanel namePanel = new JPanel(new GridLayout(1, 1));
        main.add(namePanel);
        JLabel nameLabel = new JLabel("Name : " + pokemon.getName());
        namePanel.add(nameLabel);
        nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        
        JPanel levePanel = new JPanel(new GridLayout(1, 1));
        main.add(levePanel);
        JLabel levelLabel = new JLabel("Level :" + pokemon.getLevel());
        levePanel.add(levelLabel);
        levelLabel.setHorizontalAlignment(SwingConstants.CENTER);
        
        JPanel imgPanel = new JPanel(new GridLayout(1, 1));
        main.add(imgPanel);
        imgPanel.add(pokemon.getImage());

        JPanel expPanel = new JPanel(new GridLayout(1, 1));
        main.add(expPanel);
        JLabel expLabel = new JLabel("Exp: " + pokemon.getExp() + "/" + pokemon.getLevelUpExp());
        expPanel.add(expLabel);
        expLabel.setHorizontalAlignment(SwingConstants.CENTER);

        JButton changeNameButton = new JButton("CHANGE NAME");
        main.add(changeNameButton);
        changeNameButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ChangePokemonName chnp = new ChangePokemonName(trainer, pokemon);
                setVisible(false);
            }
        });
        
        JButton backButton = new JButton("BACK");
        main.add(backButton);
        backButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ChosePokemon chp = new ChosePokemon(trainer, "checkStatus");
                setVisible(false);
            }
        });

            

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}