
import java.awt.*;
import javax.swing.ImageIcon;
import javax.swing.JLabel;


import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.*;

public abstract class Pokemon {
    protected String name;
    protected int level;
    protected int exp;
    protected int levelUpExp;
    protected int maxHp;
    protected int hp;
    protected String imageUrl;
    protected int evolveLevel;

    // public Pokemon(String name){
    //     this.name = name;
    //     this.hp = 0;
    // }

    public Pokemon(String name, int maxHp, int level){
        this.name = name;
        this.maxHp = maxHp + (int)(2 * level);
        this.hp = this.maxHp;
        this.level = level;
        this.exp = 0;
        this.levelUpExp = level * 100;
    }

    public String getName(){
        return name;
    }
    
    public int getLevel(){
        return level;
    }

    public int getMaxHp(){
        return maxHp;
    }

    public int getHp(){
        return hp;
    }

    public float getExp(){
        return exp;
    }

    public float getLevelUpExp(){
        return levelUpExp;
    }

    public String getImageUrl(){
        return imageUrl;
    }

    public void changeName(String name) {
        this.name = name;
    }

    public Component getImage(){
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(imageUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image dimg = img.getScaledInstance(100, 90, Image.SCALE_SMOOTH);
        ImageIcon imageIcon = new ImageIcon(dimg);
        JLabel label = new JLabel(imageIcon);
        return label;
    }
    
    public ImageIcon getImageIcon(){
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(imageUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image dimg = img.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
        ImageIcon imageIcon = new ImageIcon(dimg);
        return imageIcon;
    }

    protected int getEvolveLevel(){
        return evolveLevel;
    }

    public void damage(int damage){
        hp = hp - damage;
        if(hp<=0){
            hp = 0;
        }
    }

    public boolean isAlive(){
        if(hp == 0){
            return false;
        }
        return true;
    }

    public  Boolean canEvolve(){
        if(level >= evolveLevel && evolveLevel != 0){
            return true;
        }
        return false;
    }

    public boolean isWinner(Pokemon enemy){
        if(enemy.isAlive()){
            return false;
        }
        expUp(enemy);
        System.out.println(name + " is Winner");
        return true;
    }

    private void expUp(Pokemon enemy){
        exp = exp + (int)(enemy.getLevel() * 40);
        if(exp >= levelUpExp){
            int num = (int)(exp / levelUpExp);
            levelUp(num);
            exp = exp % levelUpExp;
            levelUpExp = level * 100;
        }
    }

    public void choiceRandom(Pokemon enemy){
        int num = (int)Math.random() * 3;
        if(num >= 0 && num <= 2){
            this.attack(enemy);
        }
        else if(num == 3){
            this.selfHeal();
        }
    }

    protected void heal(int value){
        hp += value;
        if(hp > maxHp){
            hp = maxHp;
        }
    }

    protected abstract Pokemon evolve();
    protected abstract void levelUp(int num);
    public abstract void selfHeal();
    public abstract void attack(Pokemon enemy);

    public String toString(){
        return name;
    }
}