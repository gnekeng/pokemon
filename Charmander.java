
public class Charmander extends Pokemon {
    public Charmander(String name){
        super(name, (int)(Math.random() * 25) + 1, (int)(Math.random() * 16 ) + 1);
        imageUrl = "charmander.png";
        evolveLevel = 16;
    }

    public Charmander(String name, int maxHp, int level){
        super(name, maxHp, level);
        imageUrl = "charmander.png";
        evolveLevel = 16;
    }
    
    protected  Pokemon evolve(){
        Charmeleon charmeleon = new Charmeleon(this.getName(), this.getMaxHp(), this.getLevel());
        System.out.println(this.getName() + "Evolvutions");
        return charmeleon;
    }
    
    public void levelUp(int num){
        level = level + 2*num;
        maxHp = maxHp + 2*num;
        hp = hp + 2*num;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Level UP!");
    }

    public void selfHeal(){
        int value = (int)(level + maxHp * (1/10));
        hp = hp + value;
        if(hp >= maxHp){
            hp = maxHp;
        }
        System.out.println(this.getName() + "Self Heal!");
    }

    public void attack(Pokemon enemy){
        System.out.println("Pokemon " + name + " attack " + enemy.getName());
        enemy.damage(8 + (int)(0.5 * level));   
    }
}