
import java.util.*;

public class PokemonRandomizer {
    public static Pokemon getPokemon(){
        Pokemon pokemon = null;
        int type = (int)(Math.random() * 99);
        if(type >= 0 && type < 25){
            pokemon = new Pikachu("Wild Pikachu");
        }
        else if(type >= 25 && type < 40){
            pokemon = new Raichu("Wild Raichu");
        }
        else if(type >= 40 && type < 50){
            pokemon = new Kabigon("Wild Kabigon");
        }
        else if(type >= 50 && type < 75){
            pokemon = new Charmander("Wild Charmander");
        }
        else if(type >= 75 && type < 90){
            pokemon = new Charmeleon("Wild Charmeleon");
        }
        else if(type >= 90 && type < 100){
            pokemon = new Charizard("Wild Charizard");
        }
        return pokemon;
    }
}