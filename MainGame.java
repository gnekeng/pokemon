
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainGame extends JFrame{
    private Trainer trainer;
    public MainGame(Trainer trainer){
        super("Pokemon Game");
        this.trainer = trainer;

        
        Container c = getContentPane();
        c.setLayout(new GridLayout(3, 2));

        JLabel trainerNameLabel = new JLabel(this.trainer.getName());
        JLabel trainerCoinLabel = new JLabel("Coin: " + Integer.toString(this.trainer.getCoin()));
        JLabel fillLabel = new JLabel("");

        JButton quitButton = new JButton("QUIT");
        JButton pokemonButton = new JButton("Pokemon Status");
        JButton findPokemoButton = new JButton("Go to Wild");
        JButton storeButton = new JButton("Go to Store");
        JButton goHospitalButton = new JButton("Go Hospital");
        JButton bagButton = new JButton("BAG");
        
        c.add(trainerNameLabel);
        c.add(trainerCoinLabel);
        c.add(fillLabel);
        c.add(pokemonButton);
        c.add(findPokemoButton);
        c.add(storeButton);
        c.add(goHospitalButton);
        c.add(bagButton);
        c.add(quitButton);

        pokemonButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                ChosePokemon chp = new ChosePokemon(trainer, "checkStatus");
                setVisible(false);
            }
        });

        findPokemoButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                trainer.meetPokemon();
                ChosePokemon chp = new ChosePokemon(trainer, "choseBattle");
                setVisible(false);
            }
        });

        bagButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                OpenBag opb = new OpenBag(trainer, "outBattle");
                setVisible(false);
            }   
        });

        storeButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Store st = new Store(trainer);
                setVisible(false);
            }
        });

        goHospitalButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                trainer.goHospital(trainer.getPokemons());
                setVisible(false);
            }
        });

        quitButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.exit(0);
            }
        });
        
        trainerNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        trainerCoinLabel.setHorizontalAlignment(SwingConstants.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);
    }
}